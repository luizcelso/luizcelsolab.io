---
layout: page
title: Sobre a disciplina
permalink: /about/
---




## Caracterização do projeto
Título: Ciência de Dados e suas aplicações em cenários de pandemia

Carga horária total: 72h

Coordenador/Responsável pela proposta: Luiz Celso Gomes Junior (DAINF)

Período de execução: 17/09/2020 a 12/02/2021

Público atendido: Alunos de graduação de Sistemas de Informação e Engenharia da Computação; Alunos de mestrado do PPGCA e CPGEI; Alunos da UNICAMP.

Colaboradores internos: Ricardo Luders (DAINF)

Colaboradores externos (se houver): A disciplina será oferecida conjuntamente no Instituto de Computação (UNICAMP) pelo Prof. André Santanchè. Outros professores da UNICAMP e UTFPR serão convidados, assim como pesquisadores especialistas nos temas abordados na disciplina.



## Resumo do projeto
O cenário atual da crise relacionada com a pandemia de COVID-19 demanda análises complexas, usando dados de diversas fontes e integrando especialistas de diferentes disciplinas. A Ciência de Dados é uma das áreas contribuindo para uma melhor compreensão do problema e na busca de estratégias de mitigação das consequências.

Este projeto visa cobrir uma ampla gama de tópicos de Ciência de Dados e aplicá-los em tarefas relacionadas com cenários de pandemia. Os alunos terão a oportunidade de aplicar os conhecimentos adquiridos em diversas questões relacionadas com a pandemia atual e as do passado, como padrões de evolução epidemiológica, impactos econômicos e sociais, comportamento da população, caracterização de aspectos de medicina e saúde pública, etc. 

O conteúdo teórico será apresentado em vídeos, disponibilizados para que os participantes assistam com flexibilidade de horários. O conteúdo será discutido em canais síncronos e assíncronos. A aplicação do conteúdo será em projetos práticos implementados em grupos e acompanhados pelos professores.

 



## Objetivos
Ensinar os principais conceitos de Ciência de Dados com aplicação prática no cenário diverso e atual de pandemia. Uma ênfase especial será dada em:



*   Complexidade do domínio e importância de inferências bem fundamentadas
*   Multidisciplinaridade
*   Diversidade de modelos



## Justificativas
Este é um conteúdo importante para a formação de alunos de computação e a aplicação no cenário da pandemia oferece uma boa oportunidade para se desenvolver o raciocínio crítico e experimentar a complexidade frequentemente presente em tarefas de Ciência de Dados.

O oferecimento do conteúdo no formato online permite a participação de alunos independentemente da sua localização física e abre a oportunidade de envolver outros professores e pesquisadores.



## Conteúdo programático


*   Introdução e Contextualização: Motivação, exemplos, história, contexto e visão geral de tecnologias relacionadas.
*   Obtenção e Análise de Dados: Técnicas para a obtenção, limpeza e processamento de dados.
*   Tópicos em modelagem estatística: conceitos básicos, projeto de experimentos e interpretação de resultados.
*   Visualização de Dados: Principais técnicas para a apresentação visual de dados e resultados. 
*   Tópicos em aprendizado de máquina: aprendizado supervisionado e aprendizado não supervisionado.
*   Manipulação de Dados em Grande Escala: Limitações do Modelo Relacional, Bancos de Dados Paralelos e Distribuídos, Bancos NoSQL, MapReduce, Modelos de Armazenamento e Linguagens de Processamento Distribuído
*   Modelagem de alta dimensionalidade: Ciência de Redes, Propriedades das Redes Complexas, Tópicos em processamento de linguagem natural
*   Tópicos em epidemiologia e assuntos relacionados: conceitos importantes e trabalhos realizados por outros pesquisadores na área.
*   Ética em Ciência



## Modalidade, Metodologia e Infraestrutura

### Aulas

As aulas se desenvolverão no ambiente virtual Google Classroom de forma assíncrona, mas alunos terão a opção de participar de conferências de debate e atendimento online.



*   Vídeos serão postados online no início de cada semana.
*   Sessões para debates sobre o tema de forma síncrona terças e quintas, das 16:00h às 18:00h.
*   Palestrantes convidados participarão com vídeo gravado ou apresentação síncrona, seguido de discussão por videoconferência. 

Também haverá oportunidade de se tirar dúvidas e participar de debates de forma assíncrona no ambiente online.


### Presença

Obrigatória em, no mínimo, 75% das aulas. 


### Google Classroom

Avisos, notas de aula, conteúdos de leitura suplementar, listas de exercício, resultados de avaliações e todas as atividades de participação serão compartilhadas no ambiente Google Classroom. 


### Atividades Avaliativas


<table>
  <tr>
   <td>Legenda
   </td>
   <td>Descrição
   </td>
   <td>Peso
   </td>
   <td>Quando ocorre
   </td>
  </tr>
  <tr>
   <td>Tp
   </td>
   <td>Entregas Parciais do Trabalho
   </td>
   <td>30%
   </td>
   <td>Ao longo do semestre
   </td>
  </tr>
  <tr>
   <td>Tf
   </td>
   <td>Avaliação Projeto Final \
 (Apresentação + Artigo + Código)
   </td>
   <td>70%
   </td>
   <td>A partir de 15/12
   </td>
  </tr>
</table>


As atividades avaliativas serão realizadas em grupos. Todos os membros dos grupos devem ter um usuário no GitLab. Todo o desenvolvimento deve usar o git/GitLab. A participação dos membros do grupo será avaliada de acordo com os logs de commits no repositório. Integrantes com participação desigual poderão ter suas notas finais ajustadas de acordo.



## Cronograma
Divulgação e inscrições: 01/08 a 15/09

Início do projeto: 17/09

Finalização e redação do relatório: 01/02/21

Entrega relatório final: 10/02/21


## Inscrições

Alunos interessados devem manifestar o interesse preenchendo o formulário no link:

[https://forms.gle/JXMw5BcXjbKjay4X6](https://forms.gle/JXMw5BcXjbKjay4X6)
